package com.shubham.kafkafirst.kafka.tutorial1;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.stream.IntStream;

public class ProducerDemoWithCallback {

    public static final Logger log = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

    public static final String LOCALHOST_9092 = "localhost:9092";

    public static void main(String[] args) {
        // create producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, LOCALHOST_9092);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create producer
        KafkaProducer<String, String> producer = new KafkaProducer<>(properties);

        IntStream.range(0, 10).forEach(index -> {
            // create producer record
            ProducerRecord<String, String> producerRecord =
                    new ProducerRecord<>("first_topic", "hello " + index);

            // send data
            producer.send(producerRecord, (metadata, exception) -> {
                // executes everytime a record is successfully sent or an exception is thrown
                if (exception == null) {
                    log.info("Successful\n"
                            + "Topic: " + metadata.topic() + "\n"
                            + "Partition: " + metadata.partition() + "\n"
                            + "Offset: " + metadata.offset() + "\n"
                            + "timestamp: " + metadata.timestamp());
                } else {
                    log.error("The error: ", exception);
                }
            });
        });

        // flush data
        producer.flush();

        // flush and close producer
        producer.close();
    }
}
