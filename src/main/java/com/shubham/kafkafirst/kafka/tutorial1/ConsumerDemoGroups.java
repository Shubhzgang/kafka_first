package com.shubham.kafkafirst.kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ConsumerDemoGroups {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerDemoGroups.class);

    public static void main(String[] args) {
        String bootstrapServer = "localhost:9092";
        String groupId = "my-fifth-application";
        String topic = "first_topic";
        Properties properties = new Properties();

        // create consumer config
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);

        // subscribe the consumer to topic(s)
        consumer.subscribe(Collections.singleton(topic));

        // poll for data
        while(true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100L));

            for (ConsumerRecord<String, String> record : records) {
                LOGGER.info("The record is as below:");
                LOGGER.info("Key: " + record.key());
                LOGGER.info("Value: " + record.value());
                LOGGER.info("Partition: " + record.partition());
                LOGGER.info("Offset: " + record.offset());
            }
        }
    }
}
