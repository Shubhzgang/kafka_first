package com.shubham.kafkafirst.kafka.tutorial1;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ConsumerDemoAssignSeek {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerDemoAssignSeek.class);

    public static void main(String[] args) {
        String bootstrapServer = "localhost:9092";
        String topic = "first_topic";
        Properties properties = new Properties();

        // create consumer config
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        // create consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);

        // assign and seek are used to replay data or get a specific message
        TopicPartition partition = new TopicPartition(topic, 0);

        // assign
        consumer.assign(Collections.singleton(partition));

        // seek
        long offset = 15L;
        consumer.seek(partition, offset);

        int numberOfMessagesToRead = 5;
        boolean keepOnReading = true;
        int messagesReadSoFar = 0;

        // poll for data
        while(keepOnReading) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100L));

            for (ConsumerRecord<String, String> record : records) {
                messagesReadSoFar += 1;
                LOGGER.info("The record is as below:");
                LOGGER.info("Key: " + record.key());
                LOGGER.info("Value: " + record.value());
                LOGGER.info("Partition: " + record.partition());
                LOGGER.info("Offset: " + record.offset());
                if (messagesReadSoFar == numberOfMessagesToRead) {
                    keepOnReading = false;
                    break;
                }
            }
        }
    }
}
